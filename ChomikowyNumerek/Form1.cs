﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ChomikowyNumerek
{
    public partial class MainForm : Form
    {
        Random rnd = new Random();
        const string input = "Pozostale.txt";
        const string output = "Wynik Losowania.txt";
        List<string> numerki = new List<string>();
        List<string> wylosowane = new List<string>();

        public MainForm()
        {
            InitializeComponent();
        }

        void NoweNumerki()
        {
            numerki.Clear();
            for (int i = 1; i <= 35; i++)
                numerki.Add(i.ToString());
            File.WriteAllLines(input, numerki.ToArray());
        }

        void WczytajNumerki()
        {
            if (!File.Exists(input))
                NoweNumerki();
            else
                numerki = new List<string>(File.ReadAllLines(input));
            if (numerki.Count < 1) NoweNumerki();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            WczytajNumerki();
        }

        int LosujNumerek()
        {
            int los = rnd.Next(numerki.Count);
            while (wylosowane.Contains(los.ToString()))
                los = rnd.Next(numerki.Count);
            wylosowane.Add(los.ToString());
            return los;
        }

        private void bLosuj_Click(object sender, EventArgs e)
        {
            wylosowane.Clear();
            tbPn.Text = numerki[LosujNumerek()];
            tbWt.Text = numerki[LosujNumerek()];
            tbSr.Text = numerki[LosujNumerek()];
            tbCz.Text = numerki[LosujNumerek()];
            tbPt.Text = numerki[LosujNumerek()];
        }

        private void bZapisz_Click(object sender, EventArgs e)
        {
            if(File.Exists(output)) File.Delete(output);
            string outputText = lPn.Text + " " + tbPn.Text + "\r\n" +
                lWt.Text + " " + tbWt.Text + "\r\n" +
                lSr.Text + " " + tbSr.Text + "\r\n" +
                lCz.Text + " " + tbCz.Text + "\r\n" +
                lPt.Text + " " + tbPt.Text + "\r\n";
            File.WriteAllText(output,outputText);

            numerki.Remove(tbPn.Text);
            numerki.Remove(tbWt.Text);
            numerki.Remove(tbSr.Text);
            numerki.Remove(tbCz.Text);
            numerki.Remove(tbPt.Text);

            File.Delete(input);
            if (numerki.Count > 0) File.WriteAllLines(input, numerki.ToArray());
            WczytajNumerki();

            Process.Start("notepad",output);
        }
    }
}

﻿namespace ChomikowyNumerek
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bLosuj = new System.Windows.Forms.Button();
            this.tbPn = new System.Windows.Forms.TextBox();
            this.lPn = new System.Windows.Forms.Label();
            this.bZapisz = new System.Windows.Forms.Button();
            this.lWt = new System.Windows.Forms.Label();
            this.lSr = new System.Windows.Forms.Label();
            this.lCz = new System.Windows.Forms.Label();
            this.lPt = new System.Windows.Forms.Label();
            this.tbWt = new System.Windows.Forms.TextBox();
            this.tbSr = new System.Windows.Forms.TextBox();
            this.tbPt = new System.Windows.Forms.TextBox();
            this.tbCz = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bLosuj
            // 
            this.bLosuj.Location = new System.Drawing.Point(131, 9);
            this.bLosuj.Name = "bLosuj";
            this.bLosuj.Size = new System.Drawing.Size(75, 44);
            this.bLosuj.TabIndex = 0;
            this.bLosuj.Text = "Losuj Numerki";
            this.bLosuj.UseVisualStyleBackColor = true;
            this.bLosuj.Click += new System.EventHandler(this.bLosuj_Click);
            // 
            // tbPn
            // 
            this.tbPn.Location = new System.Drawing.Point(88, 11);
            this.tbPn.Name = "tbPn";
            this.tbPn.ReadOnly = true;
            this.tbPn.Size = new System.Drawing.Size(26, 20);
            this.tbPn.TabIndex = 1;
            this.tbPn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lPn
            // 
            this.lPn.Location = new System.Drawing.Point(12, 9);
            this.lPn.Name = "lPn";
            this.lPn.Size = new System.Drawing.Size(70, 22);
            this.lPn.TabIndex = 2;
            this.lPn.Text = "Poniedzialek:";
            this.lPn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bZapisz
            // 
            this.bZapisz.Location = new System.Drawing.Point(131, 59);
            this.bZapisz.Name = "bZapisz";
            this.bZapisz.Size = new System.Drawing.Size(75, 60);
            this.bZapisz.TabIndex = 3;
            this.bZapisz.Text = "Zapisz Wynik Losowania";
            this.bZapisz.UseVisualStyleBackColor = true;
            this.bZapisz.Click += new System.EventHandler(this.bZapisz_Click);
            // 
            // lWt
            // 
            this.lWt.Location = new System.Drawing.Point(12, 31);
            this.lWt.Name = "lWt";
            this.lWt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lWt.Size = new System.Drawing.Size(70, 22);
            this.lWt.TabIndex = 4;
            this.lWt.Text = "Wtorek:";
            this.lWt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lSr
            // 
            this.lSr.Location = new System.Drawing.Point(12, 53);
            this.lSr.Name = "lSr";
            this.lSr.Size = new System.Drawing.Size(70, 22);
            this.lSr.TabIndex = 5;
            this.lSr.Text = "Sroda:";
            this.lSr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lCz
            // 
            this.lCz.Location = new System.Drawing.Point(12, 75);
            this.lCz.Name = "lCz";
            this.lCz.Size = new System.Drawing.Size(70, 22);
            this.lCz.TabIndex = 6;
            this.lCz.Text = "Czwartek:";
            this.lCz.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lPt
            // 
            this.lPt.Location = new System.Drawing.Point(12, 97);
            this.lPt.Name = "lPt";
            this.lPt.Size = new System.Drawing.Size(70, 22);
            this.lPt.TabIndex = 7;
            this.lPt.Text = "Piatek:";
            this.lPt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbWt
            // 
            this.tbWt.Location = new System.Drawing.Point(88, 33);
            this.tbWt.Name = "tbWt";
            this.tbWt.ReadOnly = true;
            this.tbWt.Size = new System.Drawing.Size(26, 20);
            this.tbWt.TabIndex = 8;
            this.tbWt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbSr
            // 
            this.tbSr.Location = new System.Drawing.Point(88, 55);
            this.tbSr.Name = "tbSr";
            this.tbSr.ReadOnly = true;
            this.tbSr.Size = new System.Drawing.Size(26, 20);
            this.tbSr.TabIndex = 9;
            this.tbSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbPt
            // 
            this.tbPt.Location = new System.Drawing.Point(88, 99);
            this.tbPt.Name = "tbPt";
            this.tbPt.ReadOnly = true;
            this.tbPt.Size = new System.Drawing.Size(26, 20);
            this.tbPt.TabIndex = 11;
            this.tbPt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbCz
            // 
            this.tbCz.Location = new System.Drawing.Point(88, 77);
            this.tbCz.Name = "tbCz";
            this.tbCz.ReadOnly = true;
            this.tbCz.Size = new System.Drawing.Size(26, 20);
            this.tbCz.TabIndex = 10;
            this.tbCz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 131);
            this.Controls.Add(this.tbPt);
            this.Controls.Add(this.tbCz);
            this.Controls.Add(this.tbSr);
            this.Controls.Add(this.tbWt);
            this.Controls.Add(this.lPt);
            this.Controls.Add(this.lCz);
            this.Controls.Add(this.lSr);
            this.Controls.Add(this.lWt);
            this.Controls.Add(this.bZapisz);
            this.Controls.Add(this.lPn);
            this.Controls.Add(this.tbPn);
            this.Controls.Add(this.bLosuj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Chomikowy Numerek";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bLosuj;
        private System.Windows.Forms.TextBox tbPn;
        private System.Windows.Forms.Label lPn;
        private System.Windows.Forms.Button bZapisz;
        private System.Windows.Forms.Label lWt;
        private System.Windows.Forms.Label lSr;
        private System.Windows.Forms.Label lCz;
        private System.Windows.Forms.Label lPt;
        private System.Windows.Forms.TextBox tbWt;
        private System.Windows.Forms.TextBox tbSr;
        private System.Windows.Forms.TextBox tbPt;
        private System.Windows.Forms.TextBox tbCz;
    }
}

